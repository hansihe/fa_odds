defmodule FaOdds.Feed.Oddball do
  @behaviour FaOdds.Feed

  defstruct feed_id: nil, name: nil, kickoff: nil, outcomes: nil

  def fetch do
    odds_url = "https://s3-eu-west-1.amazonaws.com/fa-oddball/worktest/ag.json"
    %HTTPoison.Response { status_code: 200, body: body } = HTTPoison.get!(odds_url)
    Poison.decode!(body)
  end

  defp preprocess_entry({id, entry}) do
    outcomes = entry["outcomes"]
    |> Enum.map(fn([_pred_id, outcome, factor]) -> {outcome, factor} end)
    |> Enum.into(%{})

    %__MODULE__{
      feed_id: id,
      name: entry["name"],
      kickoff: entry["kickOf"],
      outcomes: outcomes,
    }
  end

  def preprocess(raw) do
    raw
    |> Enum.map(&preprocess_entry/1)
    |> Enum.filter(&FaOdds.Util.has_keys?(&1.outcomes, ["1", "x", "2"]))
  end

  # 25 hours
  # This should cover all possible timezone variation
  # (presuming that's actually the problem), plus some
  # extra.
  @match_interval 60*60*25

  # This has been chosen completely arbitrarily.
  # TODO: Have a look at how this performs
  @similarity_threshold 0.95

  defp calc_similarity({team1, team2}, to_test) do
    # Could add (potentially?) fast path for exact match
    # as it seems to be common.
    Enum.max([
      String.jaro_distance("#{team1} - #{team2}", to_test),
      String.jaro_distance("#{team2} - #{team1}", to_test),
    ])
  end

  defp find_schedule_match(entry) do
    # Find all the games on the schedule with a kickoff that's within
    # the given interval.
    schedule_entries = FaOdds.Schedule.query_by_kickoff(
      entry.kickoff, @match_interval)

    {similarity, schedule_entry} = schedule_entries
    |> Enum.map(fn(sched) ->
      {
        calc_similarity({sched["team1_name"], sched["team2_name"]}, entry.name),
        sched,
      }
    end)
    |> Enum.max_by(&elem(&1, 0), fn -> {0, nil} end)

    if similarity > @similarity_threshold do
      {:ok, schedule_entry}
    else
      :nomatch
    end
  end

  defp make_feed_entry({:ok, schedule_entry}, feed_data) do
    %FaOdds.Feed.Entry {
      feed: __MODULE__,

      id: schedule_entry["id"],
      team1: schedule_entry["team1_name"],
      team2: schedule_entry["team2_name"],

      feed_id: feed_data.feed_id,
      outcomes: feed_data.outcomes,
    }
  end
  defp make_feed_entry(:nomatch, _), do: nil

  def match_schedule(entry) do
    find_schedule_match(entry)
    |> make_feed_entry(entry)
  end

end

defmodule FaOdds.Feed.Entry do

  defstruct id: nil, team1: nil, team2: nil, outcomes: nil, feed: nil, feed_id: nil

  def store(entry) do
    s3_bucket = Application.get_env(:fa_odds, :s3_bucket)

    encoded = Poison.encode!(entry)
    ExAws.S3.put_object(s3_bucket, "#{entry.id}.json", encoded)
    |> ExAws.request!
  end

end

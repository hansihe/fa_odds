defmodule FaOdds.Feed.Runner do
  use GenServer
  require Logger

  def start_link(feeds) do
    GenServer.start_link(__MODULE__, feeds)
  end

  def init(feeds) do
    FaOdds.Schedule.ensure_fetched

    send self(), :run

    {:ok, feeds}
  end

  def handle_info(:run, feeds) do
    Logger.info fn -> "Start fetching feeds: #{inspect feeds}" end

    FaOdds.Feed.make_flow(feeds)
    |> Flow.run

    Logger.info fn -> "Finish fetching feeds: #{inspect feeds}" end

    refresh_interval = Application.get_env(:fa_odds, :feed_refresh_interval)
    :timer.send_after(refresh_interval, :run)

    {:noreply, feeds}
  end

end

defmodule FaOdds.Util do

  def has_keys?(map, keys) do
    has_keys?(map, keys, true)
  end

  defp has_keys?(_, _, false), do: false
  defp has_keys?(_, [], true), do: true
  defp has_keys?(map, [key | keys], true) do
    has_keys?(map, keys, Map.has_key?(map, key))
  end

end

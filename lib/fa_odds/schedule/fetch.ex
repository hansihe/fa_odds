defmodule FaOdds.Schedule.Fetch do

  @keys [:id, :kickoff_at, :team1_id, :team1_name, :team2_id, :team2_name,
         :tournament_id]

  def fetch(last_checked \\ "") do
    # I am doing it this way for simplicity. For anything larger you
    # would probably want to use a url formatting function.
    url = "http://Tengu:Sciopod@test.footballaddicts.com/test_assignment/match_schedule?last_checked_at=#{last_checked}"
    headers = [{"Accept", "application/json"}]

    response = HTTPoison.get!(url, headers)
    %HTTPoison.Response { status_code: 200, body: body } = response

    Poison.Parser.parse!(body, keys: :atoms!)
  end

end

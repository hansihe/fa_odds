defmodule FaOdds.Feed do

  @type raw_feed_entry :: struct

  # This should fetch the raw data from the feed source and return
  # it relatively unchanged.
  # This is done to make testing of the preprocess function a bit
  # easier.
  @callback fetch() :: any

  # This should take the raw data returned by fetch and do any
  # required preprocessing and validation. The values that are
  # returned MUST be an instance of the feed struct.
  # The flow runner uses the struct field in the returned structs
  # to call the match_schedule function on the module.
  @callback preprocess(any) :: [raw_feed_entry]

  # Attempts to match the raw feed entry with a match from the
  # schedule. If no match can be found, this should return nil.
  @callback match_schedule(raw_feed_entry) :: %FaOdds.Feed.Entry{} | nil

  defp raw_entry_do(entry, fun_name) do
    apply(entry.__struct__, fun_name, [entry])
  end

  defp entries_for_feed(feed) do
    raw = apply(feed, :fetch, [])
    apply(feed, :preprocess, [raw])
  end

  def make_flow(feeds) do
    Flow.from_enumerable(feeds)

    # This will do the network request and fetch the batch
    # of data from the feed.
    |> Flow.flat_map(&entries_for_feed(&1))
    |> Flow.partition

    # This stage takes the feed specific structs from the
    # previous stage and matches them to the schedule.
    # This will also convert from our feed specific struct
    # to our universal FaOdds.FeedEntry struct.
    |> Flow.map(&raw_entry_do(&1, :match_schedule))
    # ... we then filter out the entries we didn't find
    # on the schedule.
    |> Flow.filter(&(&1 != nil))

    # Store everything to S3
    # TODO: Would probably want to handle errors here,
    # when doing a large amount of S3 requests, we are
    # going to have one fail sooner or later.
    |> Flow.each(&FaOdds.Feed.Entry.store(&1))
  end

  def run(feeds) do
    make_flow(feeds)
    |> Flow.run
  end

end

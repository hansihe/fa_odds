defmodule FaOdds.Schedule do
  use GenServer
  require Logger

  @tab_name __MODULE__

  # Client

  def start_link(opts) do
    start_res = GenServer.start_link(__MODULE__, nil, [{:name, __MODULE__} | opts])
    with {:ok, pid} <- start_res do
      send pid, :refresh
    end
    start_res
  end

  def ensure_fetched do
    GenServer.call(__MODULE__, :ensure_fetched)
  end

  # I can't query for team names directly because I want to
  # do more fuzzy matching.
  def schedule do
    spec = [
      {
        {:_, :'$1'},
        [],
        [:'$1']
      }
    ]
    :ets.select(@tab_name, spec)
  end

  def query_by_kickoff(kickoff, within_seconds \\ 0) do
    spec = [
      {
        {:_, %{ :kickoff_at => :'$1' }},
        [{:andalso,
          {:'<', :'$1', kickoff + within_seconds},
          {:'>', :'$1', kickoff - within_seconds}}],
        [{:element, 2, :'$_'}]
      }
    ]
    :ets.select(@tab_name, spec)
  end

  # Server

  def init(nil) do
    :ets.new(@tab_name, [{:read_concurrency, true}, :named_table])

    timestamp = ""
    {:ok, timestamp}
  end

  # Because the :refresh message was sent before the start_link
  # function returned, it should be first in the message queue.
  # Simply replying here will ensure that the caller waits until
  # we have refreshed.
  def handle_call(:ensure_fetched, _from, state) do
    {:reply, :ok, state}
  end

  def handle_info(:refresh, last_fetched) do
    # I noticed I get only new matches when querying with
    # last_fetched.
    # I just merge them with the old ones, and then purge
    # the matches that have started.
    # Is this how it should be done?

    # To avoid matches being missed, it might be better to use
    # a timestamp obtained from the server. For simplicity I
    # use the local timestamp minus a couple of seconds here.
    timestamp = :os.system_time(:seconds) - 60

    Logger.info fn -> "Start updating schedule" end

    # Fetch and insert new matches
    fetched_schedule = FaOdds.Schedule.Fetch.fetch(last_fetched)
    Enum.each(fetched_schedule, fn(entry) ->
      :ets.insert(@tab_name, {entry.id, entry})
    end)

    # Purge out old matches

    # Purge matches that kicked off more than 2 hours ago.
    # Keep in mind I have no idea if what I'm doing here is
    # technically right, as I am not very familiar with football
    # betting.
    purge_point = :os.system_time(:seconds) - 60*60*2
    delete_spec = [
      {
        {:_, %{"kickoff_at" => :'$1'}},
        [{:'<', :'$1', purge_point}],
        [true]
      }
    ]
    :ets.select_delete(@tab_name, delete_spec)

    Logger.info fn -> "Finish updating schedule" end

    refresh_interval = Application.get_env(:fa_odds, :schedule_refresh_interval)
    :timer.send_after(refresh_interval, :refresh)

    {:noreply, timestamp}
  end

  defp lookup(key) do
    [{^key, value}] = :ets.lookup(@tab_name, key)
    value
  end

end

defmodule FaOdds.Mixfile do
  use Mix.Project

  def project do
    [
      app: :fa_odds,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison, :hackney, :ex_aws],
      mod: {FaOdds.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 0.13"},
      {:poison, "~> 3.1"},
      {:ex_aws, "~> 1.1"},
      {:sweet_xml, "~> 0.6.5"},
      {:gen_stage, "~> 0.12"},
      {:flow, "~> 0.12"},
    ]
  end
end

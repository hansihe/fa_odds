defmodule FaOddsTest do
  use ExUnit.Case
  doctest FaOdds

  @test_odds %{
  "2012477" => %{"kickOf" => 1504022400, "name" => "US Catanzaro - Rende",
    "outcomes" => [["11898961", "1", "2.0"], ["129337803", "2", "3.3"],
     ["45266644", "x", "3.25"]]},
  "60811745" => %{"kickOf" => 1504002600,
    "name" => "Hakoah - Melbourne City FC",
    "outcomes" => [["83552389", "1", "67.0"], ["41616837", "2", "1.0"],
     ["58090567", "x", "51.0"]]},
  "102448857" => %{"kickOf" => 1504024200, "name" => "Dalum IF - Odense BK",
    "outcomes" => [["19554074", "x", "7.4"]]},
  "59952572" => %{"kickOf" => 1504029600,
    "name" => "Motherwell FC - Aberdeen FC",
    "outcomes" => [["126111401", "1", "1.65"], ["91781931", "2", "4.0"],
     ["2595192", "x", "3.8"]]},
  }
  test "odds processing" do
    res = FaOdds.Feed.Oddball.preprocess(@test_odds)
    assert Enum.count(res) == 3
  end

  test "complete feed processing flow" do
    FaOdds.Schedule.ensure_fetched

    FaOdds.Feed.run([FaOdds.Feed.Oddball])
  end

end
